#pragma checksum "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8d536ef6d87e1413373d496a0b129618a90ff578"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Product_Index), @"mvc.1.0.view", @"/Views/Product/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Product/Index.cshtml", typeof(AspNetCore.Views_Product_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8d536ef6d87e1413373d496a0b129618a90ff578", @"/Views/Product/Index.cshtml")]
    public class Views_Product_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<ASPProduct.Models.Product>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
            BeginContext(81, 26, true);
            WriteLiteral("\r\n<h1>ProductView</h1>\r\n\r\n");
            EndContext();
#line 8 "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml"
 foreach (var item in Model)
{

#line default
#line hidden
            BeginContext(140, 61, true);
            WriteLiteral("    <table>\r\n        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(202, 7, false);
#line 13 "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml"
           Write(item.Id);

#line default
#line hidden
            EndContext();
            BeginContext(209, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(265, 9, false);
#line 16 "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml"
           Write(item.Name);

#line default
#line hidden
            EndContext();
            BeginContext(274, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(330, 16, false);
#line 19 "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml"
           Write(item.Description);

#line default
#line hidden
            EndContext();
            BeginContext(346, 50, true);
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n    </table>\r\n");
            EndContext();
#line 23 "C:\Users\AD073526\Desktop\C#\ASPProduct\ASPProduct\Views\Product\Index.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<ASPProduct.Models.Product>> Html { get; private set; }
    }
}
#pragma warning restore 1591
