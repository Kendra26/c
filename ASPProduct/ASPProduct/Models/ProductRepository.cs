﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPProduct.Models
{

    public interface IProductRepository
    {
        Product GetById(int id);
        IEnumerable<Product> GetAll();
    }

    public class ProductRepository: IProductRepository
    {
        
        readonly List<Product> _prod = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Milk",
                Description="3% fat"
            },
                new Product
            {
                Id=2,
                Name="Rice",
                Description="long grain"
            },
        };

        public List<Product> Products => _prod;

       


        public Product GetById(int id)
        {
            foreach (var p in _prod)
            {
                if (p.Id == id)
                {
                    return p;
                }
            }

            return null;
        }

        public IEnumerable<Product> GetAll() => _prod;

    }
}

