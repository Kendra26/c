﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASPProduct.Models;

namespace ASPProduct.Controllers
{
    public class ProductController : Controller
    {
        readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
            
        }

        public ProductController()
        {
            _productRepository = new ProductRepository();
        }

        readonly ProductRepository _repo;

        public ProductController(ProductRepository repo)
        {
            _repo = repo;
        }


        public Product GetById(int id)
        {
            return _repo.GetById(id);
           
        }

        public IEnumerable<Product> GetAll() => _repo.GetAll();



        public IActionResult Index()
        {
            var products = _repo.Products;
            return View(products);
        }
    }
}