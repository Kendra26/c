﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zoo;

namespace AnimalMainBecauseClassLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal a = new Animal
            {
                Id = 1,
                Name = "Cleo",
                Description = "Sophisticated"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Rocky",
                Description = "Passive agressive "
            };

            Animal created = new Animal(3, "Coco", "loud ");
            Animal created2= new Animal(4, "Ricky", "had some issues ");
            Animal created3 = new Animal(5, "Foxy", "fishy ");

            AnimalRepo repo = new AnimalRepo();

            repo.Add(a);
            repo.Add(a2);
            repo.Add(created);
            repo.Add(created2);
            repo.Add(created3);

          //  repo.Remove(2);
          //  repo.editAnimal(1, 6,"Ricky", "the pigeon");

            Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            IEnumerable<Animal> animals = repo.GetAll();

            //Console.WriteLine(first);
            //Console.WriteLine(second);


            foreach (Animal ani in animals)
            {
                Console.WriteLine(ani);
            }

        }
    }
}
