﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// !!!!!
using Moq;
using Zoo;
using Animal = Zoo.Animal;

namespace AnimalTests
{
    [TestClass]
    public class AnimalControllerTest
    {
        [TestMethod]
        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();

            Animal ani = new Animal
            {
                Id = 1,
                Name = "Becky",
                Description = "Wolf"
            };

            repoMock.Setup(x => x.Add(ani));

            repoMock.Setup(x => x.GetById(1)).Returns(ani);

            AnimalController controller = new AnimalController(repoMock.Object);
            Animal result = controller.GetAnimalById(1);

            Assert.AreEqual(ani.Id, result.Id);
            Assert.AreEqual(ani.Name, result.Name);
            Assert.AreEqual(ani.Description, result.Description);
        }

        [TestMethod]
        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();

            Animal a = new Animal
            {
                Id = 1,
                Name = "Cleo",
                Description = "Sophisticated"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Rocky",
                Description = "Passive agressive "
            };

            List<Animal> animalsList = new List<Animal>
            {
                a,
                a2
            };

            mock.Setup(x => x.GetAll()).Returns(animalsList);

            AnimalController controller = new AnimalController(mock.Object);


            //??
         //   List<Animal> result = controller.GetAll().ToList();

        //    Assert.AreEqual(2, result.Count);
        }
    }
}
