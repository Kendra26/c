﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zoo;

namespace AnimalTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class AnimalTest
    {
        Animal a = new Animal
        {
            Id = 1,
            Name = "Cleo",
            Description = "Sophisticated"
        };

        Animal a2 = new Animal
        {
            Id = 2,
            Name = "Rocky",
            Description = "Passive agressive "
        };

        [TestMethod]
        public void RetrieveCorrectData_Test()
        {
            AnimalRepo repo = new AnimalRepo();

            repo.Add(a);
            repo.Add(a2);

           Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            Assert.AreEqual(a.Id, first.Id);
            Assert.AreEqual(a.Name, first.Name);
            Assert.AreEqual(a.Description, first.Description);
            
        }

        [TestMethod]
        public void RetrieveInvalidData_Test()
        {
            AnimalRepo repo = new AnimalRepo();

            repo.Add(a);
            repo.Add(a2);

            Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            Assert.AreEqual(a.Name, first.Name);
            Assert.AreEqual(a.Description, first.Description);
            Assert.AreEqual(3, first.Id);
        }
    }
}
