﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW1
{
    class Calculator
    {
        static void Main(string[] args)
        {
            double firstNumber = 0, secondNumber = 0;
           

            Console.WriteLine("First number is: ");
            firstNumber =Convert.ToDouble (Console.ReadLine());

            Console.WriteLine("Second number is: ");
            secondNumber = Convert.ToDouble (Console.ReadLine());

            Console.WriteLine("The operation: ");
            Console.WriteLine("\t+ " + "\t-" + "\t*" + "\t /" );
         
            
            switch (Console.ReadLine())
            {
                case "+":
                    Console.WriteLine(firstNumber + secondNumber);
                    break;
                case "-":
                    Console.WriteLine(firstNumber - secondNumber);
                    break;
                case "*":
                    Console.WriteLine(firstNumber * secondNumber);
                    break;
                case "/":
                    while (secondNumber == 0)
                    {
                        Console.WriteLine("Provide a non-zero divisor: ");
                        secondNumber = Convert.ToInt32(Console.ReadLine());
                    }
                    Console.WriteLine(firstNumber / secondNumber);
                    break;

                default:
                    break;
            }
            Console.ReadKey();
            
        }
    }
}