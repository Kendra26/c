﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Persoane
{
    class Program
    {
        static void Main(string[] args)
        {
            // List<Persoana> personList = new List<Persoana>();

            //Persoana pers = new Persoana("Sara", "Smith", "Soarelui");

            // using Enumerable.Repeat
           // var personList = Enumerable.Repeat(new Persoana("John", "Snow", "Jupiter"), 1).ToList();


            var personList = new List<Persoana>() {
                new Persoana( "Snow","John", "Jupiter" ),
                new Persoana( "Fisher","Ryn","Venus" ),
                new Persoana( "Bonciu","Sarah","Neptun" ),
                new Student( "Somerhalder","Ian", "45", 9.70),
                new Student( "Pop","Antonia", "13", 8.50)
            };

            List<Persoana> SortedListByFirstName = personList.OrderBy(o => o.FirstName).ToList();
            List<Persoana> SortedListByLastName = personList.OrderBy(o => o.LastName).ToList();


            foreach (Persoana p in personList)
            {
                Console.WriteLine(p);
            }

            Console.WriteLine("=========================================");

            foreach (Persoana p in SortedListByFirstName)
            {
                Console.WriteLine(p);
            }

            Console.WriteLine("==========================================");

            foreach (Persoana p in SortedListByLastName)
            {
                Console.WriteLine(p);
            }


        }
    }
}
