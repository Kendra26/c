﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persoane
{
    class Student : Persoana
    {
        public Student(string nume, string prenume, string nr, double medi):base()
        {

            FirstName = nume;
            LastName = prenume;
            NrMatricol = nr;
            Medie = medi;

        }

        private string NrMatricol { get; set; }
        private double Medie { get; set; }
    }
}
