﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persoane
{
   public class Persoana
    { 
        public Persoana()
        {

        }

        public Persoana(string nume, string prenume,string adresa)
        {
            FirstName = nume;
            LastName = prenume;
            Address = adresa;

        }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        protected string Address { get; set; }

        public override string ToString() => $"The List Contains : {FirstName} {LastName} {Address}";

    }
}
