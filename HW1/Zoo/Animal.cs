﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Animal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Animal()
        {

        }

      public Animal(int id, string name, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
        }


       public override string ToString() => $"Animal Info: {Id} {Name} {Description}";
    }
}
