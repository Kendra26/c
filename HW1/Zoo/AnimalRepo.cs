﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class AnimalRepo : IAnimalRepository
    {
        readonly List<Animal> _animal = new List<Animal>();

        
        public void Add(Animal ani)
        {
            _animal.Add(ani);
        }

        public Animal GetById(int id)
        {
            return _animal.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Animal> GetAll() => _animal;
    
        public void Remove(int aniId)
        {
               
                var itemToRemove = _animal.SingleOrDefault(r => r.Id == aniId);
                if (itemToRemove != null)
                    _animal.Remove(itemToRemove);
         
        }
                
        public Animal editAnimal(int id,int newId , string newName, string newDescription)
        {

           foreach (Animal obj in _animal)
            {
                if (obj.Id == id)
                {
                    obj.Id = newId;
                    obj.Name = newName;
                    obj.Description = newDescription;
                    break;
                }
            }
            return null;
        }


    }
}
