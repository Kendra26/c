﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
     public interface IAnimalRepository
    {
            void Add(Animal a);
            Animal GetById(int id);
            IEnumerable<Animal> GetAll();
        
    }
}
