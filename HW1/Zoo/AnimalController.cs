﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class AnimalController
    {
        readonly IAnimalRepository _animalRepository;

        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }


        public AnimalController()
        {
            _animalRepository = new AnimalRepo();
        }

        public void Insert(Animal a)
        {
            _animalRepository.Add(a);
        }

        public Animal GetAnimalById(int id)
        {
            return _animalRepository.GetById(id);
        }

        public IEnumerable<Animal> GetAll() => _animalRepository.GetAll();

    }
}
