﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Tiger : Animal
    {
        public string Sound { get; set; }
        public string Type { get; set; }
    }
}
