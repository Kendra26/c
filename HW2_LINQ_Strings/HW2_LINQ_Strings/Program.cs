﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HW2_LINQ_Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string starts, ends;
           
            string[] countries =
            {
                "Austria","Australia","Norway","Finland","Sweden","Greece","Turkey","Germany"
            };

         
            Console.Write("\nThe countries are : 'Austria','Australia','Norway','Finland','Sweden','Greece','Turkey','Germany' \n");

            Console.Write("\nInput starting character for the string : ");
            starts = Console.ReadLine();

            
            Console.Write("\nInput ending character for the string : ");
            ends = Console.ReadLine();


            var result = from x in countries
                          where x.StartsWith(starts)
                          where x.EndsWith(ends)
                          select x;
          
            foreach (var country in result)
            {
                Console.Write("The country starting with {0} and ending with {1} is : {2} \n", starts, ends, country);
            }

        }
    }
}
*/